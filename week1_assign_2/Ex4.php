<?php
$TextFile=file_get_contents('/var/log/apache2/access.log.1');
$Rows=explode("\n",$TextFile);
$RegExpIP='/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/';
$RegExpDate='/\[\d{1,2}.[a-zA-Z]{3}.\d{1,4}/';
$RegExpTime='/\d{2}:\d{2}:\d{2}/';
$RegExpRes='/"([^"]+)" \d{1,3}/';

for ($i = 0; $i <= sizeof($Rows); $i++) {

    // Get Ip Address
    preg_match($RegExpIP, $Rows[$i], $IPaddress);
    $IP_address=$IPaddress[0];

    // Get Date + Change format
    preg_match($RegExpDate, $Rows[$i], $Date_matches);
    $Dates=substr($Date_matches[0],1);
    $DateVar=DateTime::createFromFormat('d/M/Y', $Dates);
    $DateFormat = $DateVar->format('D, M d Y');

    // Get Time
    preg_match($RegExpTime, $Rows[$i], $Times);
    $Time=$Times[0];

    // Get HTTP Response
    preg_match($RegExpRes, $Rows[$i], $HTTPResponse);
    $HTTP_response=$HTTPResponse[0];

    // Collect all
    echo $FullReport[$i]= $IP_address . " -- " . $DateFormat . " :  " . $Time . " -- " . $HTTP_response . "\n";
}

?>
