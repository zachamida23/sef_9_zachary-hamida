<?php
$dir=$argv[1];
echo 'Files within ' . $dir . "\n";

// Get files from the given directory and all sub-directories
GetAllFiles($dir);


function GetAllFiles($dir){
if(is_dir($dir)){
// Open directory and read its content
	if($dh = opendir($dir)){
	// Loop over all folders and files in the directory
    		while(($file = readdir($dh)) !== false){
    			if($file != '.' && $file != '..'){
    		     		if(!is_dir($dir . $file)){
					// File
    		  			echo $file . "\n";
    		     		}else{
					// Recursive search for files
    					GetAllFiles($dir . $file);
				}
    		 	}
    	 	}
    	 }
	// close directory
     	closedir($dh);
 }
 }
?>
