UsedRam=$(free | awk 'FNR==3 {print $3/($3+$4)*100}')
UsedDisk=$(df | grep '^/dev' | awk '{print $5/100}')
UsedDiskName=$(df | grep '^/dev' | awk '{print $1}')
Threshold=0.8
if [[ $UsedRam < $Threshold ]]; then
 echo "Everything is OK!"
else
 echo "ALARM: Virtual Memory is at $UsedRam"
fi 

if [[ $UsedDisk < $Threshold ]]; then
 echo "Everything is OK!"
else
 echo "ALARM: Disk $UsedDiskName is at $UsedDisk"
fi 

